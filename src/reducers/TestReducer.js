const DefaultState = {
    loading: false,
    data: {},
    errorMsg: "",
};

const TestReducer = (state = DefaultState, action) => {
    switch (action.type) {
        case "TEST_LOADING":
            return {
                ...state,
                loading: true,
                errorMsg: ""
            };
        case "TEST_ADD_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            };
        case "TEST_FAIL":
            return {
                ...state,
                loading: false,
                errorMsg: action.err
            }
        case "TEST_DELETE_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: state.data.filter(d => d._id !== action.payload._id)
            }
        case "TEST_UPDATED_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            }
        case "TEST_LIST_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            }
        default:
            return state;
    }
}

export default TestReducer;