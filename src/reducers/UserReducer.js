const DefaultState = {
    loading: false,
    data: {},
    errorMsg: "",
};

const UserReducer = (state = DefaultState, action) => {
    switch (action.type) {
        case "LOGIN_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            };
        
        default:
            return state;
    }
}

export default UserReducer;