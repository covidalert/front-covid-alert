import {combineReducers} from "redux";

import TestReducer from "./TestReducer";
import UserReducer from "./UserReducer";
import VaccineReducer from "./VaccineReducer";


const RootReducer = combineReducers({
    Vaccine: VaccineReducer,
    User: UserReducer,
    Test: TestReducer,
});

export default RootReducer;