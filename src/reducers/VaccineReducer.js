const DefaultState = {
    loading: false,
    data: {},
    errorMsg: "",
};

const VaccineReducer = (state = DefaultState, action) => {
    switch (action.type) {
        case "VACCINE_LOADING":
            return {
                ...state,
                loading: true,
                errorMsg: ""
            };
        case "VACCINE_ADD_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            };
        case "VACCINE_FAIL":
            return {
                ...state,
                loading: false,
                errorMsg: action.err
            }
        case "VACCINE_DELETE_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: state.data.filter(d => d._id !== action.payload._id)
            }
        case "VACCINE_UPDATED_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            }
        case "VACCINE_LIST_SUCCESS":
            return {
                ...state,
                loading: false,
                errorMsg: "",
                data: action.payload
            }
        default:
            return state;
    }
}

export default VaccineReducer;