export const login = (data) => async dispatch => {
    try {
        localStorage.setItem("user", JSON.stringify(data));
        dispatch({
            type: "LOGIN_SUCCESS",
            payload: data
        });

    } catch (err) {
        console.log(err);
    }
};

export const logout = () => async dispatch => {

    try{
        localStorage.removeItem("user");

        dispatch({
            type: "LOGOUT_SUCCESS",
        });

    } catch (e) {
        console.log(e);
    }
};