import axios from "axios";
import servURL from "../servURL";
import {authHeader} from "../utils";

export const postTest = (test) => async dispatch => {
    try {
        const res = await axios.post(`${servURL}/tests`, test,{headers: authHeader()});
        dispatch({
            type: "TEST_ADD_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};

export const getAllTest = (id_user) => async dispatch => {
    try {
        const res = await axios.get(`${servURL}/tests/user/${id_user}` ,{headers: authHeader()});
        dispatch({
            type: "TEST_LIST_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};

export const deleteTest = (id) => async dispatch => {
    try {
        const res = await axios.delete(`${servURL}/tests/${id}` ,{headers: authHeader()});
        dispatch({
            type: "TEST_DELETE_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};