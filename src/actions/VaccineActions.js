import axios from "axios";
import servURL from "../servURL";
import {authHeader} from "../utils";

export const postVaccine = (vaccine) => async dispatch => {
    try {
        const res = await axios.post(`${servURL}/vaccines`, vaccine ,{headers: authHeader()});
        console.log(res)
        dispatch({
            type: "VACCINE_ADD_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};


export const getAllVaccines = (id_user) => async dispatch => {
    try {
        const res = await axios.get(`${servURL}/vaccines/user/${id_user}` ,{headers: authHeader()});
        dispatch({
            type: "VACCINE_LIST_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};

export const deleteVaccine = (id_vaccine) => async dispatch => {
    try {
        const res = await axios.delete(`${servURL}/vaccines/${id_vaccine}` ,{headers: authHeader()});
        dispatch({
            type: "VACCINE_DELETE_SUCCESS",
            payload: res.data
        });

    } catch (err) {
        // dispatch({
        //     type: "FESTIVAL_POST_FAIL",
        //     err: err,
        // });
    }
};