import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Keycloak from 'keycloak-js';
import {login} from "../actions/UserActions";
import { Redirect } from 'react-router';

const Login = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.User);
    const [error, setError] = useState("")

    useEffect(() => {
        if(user.data && !user.data.isLoggedIn){
            const keycloak = Keycloak('/keycloak.json');
            keycloak.init({onLoad: 'login-required'}).then(authenticated => {
                if(authenticated){
                    const data = { token: keycloak.token, isLoggedIn: authenticated }
                    dispatch(login(data));
                }
            }).catch(err => setError(err.error))
         }
      });

    return(
        <div>
        {
            user.data.isLoggedIn
            ? <Redirect to="/home" />
            : <p>Veuillez patienter...</p>
            

        }
        <p>{error}</p>
        </div>
        
        
                
    );
}

export default Login;