import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Keycloak from 'keycloak-js';
import {login} from "../actions/UserActions";
import Button from '@mui/material/Button';

import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { postVaccine, getAllVaccines, deleteVaccine } from '../actions/VaccineActions';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import moment from 'moment';

const Vaccine = () => {
    const initialVaccine = {nom:"", date:""}

    const dispatch = useDispatch();
    const user = useSelector(state => state.User);
    const listVaccine = useSelector(state => state.Vaccine);
    const [open,setOpen] = useState(false)
    const [vaccine,setVaccine] = useState(initialVaccine)
    const [listVaccin,setlistVaccine] = useState([])
    
    useEffect(() => {
        if(user.data && !user.data.isLoggedIn){
            const keycloak = Keycloak('/keycloak.json');
            keycloak.init({onLoad: 'login-required'}).then(authenticated => {
                if(authenticated){
                    const data = { token: keycloak.token, isLoggedIn: authenticated, id_user: keycloak.tokenParsed.sub }
                    dispatch(login(data));
                    dispatch(getAllVaccines(data.id_user));
                }
            })
        }  
        if(Array.isArray(listVaccine.data)){
            setlistVaccine(listVaccine.data)
        }   
    });

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (evt) => {
        const { name, value } = evt.target
        setVaccine({...vaccine, [name]: value})
    }

    const handleSubmit = () => {
        console.log(vaccine)
        handleClose()
        dispatch(postVaccine({...vaccine, id_user: user.data.id_user}))
    }

    const handleDelete = (id) => {
        dispatch(deleteVaccine(id))
    }

    return (
        <div style={{textAlign:'center'}}>
            <div style={{flexDirection:'row'}}>
                <Typography variant="h5" component="div" >Mes vaccins réalisés</Typography>
                <AddIcon color="success" onClick={()=>setOpen(true)}/>
            </div>
            
            {
                (listVaccin || []).map(t => 
                    <Box
                        component="span"
                        sx={{ display: 'inline-block', mx: '5px', minWidth: '100px', minHeight:'100px'}}
                        key={t.id_vaccin}
                    >
                        <Card variant="outlined">
                        <CardContent>
                            <Typography variant="h5" component="div">
                                {t.nom}
                            </Typography>
                            <Typography color="text.secondary">
                                {moment(t.date).format("DD MMM YYYY")}
                            </Typography>
                            </CardContent>
                            <DeleteIcon color="error" onClick={() => handleDelete(t.id_vaccin)}/>
                        </Card>
    
                    </Box>
                )
            }
            <br/>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                {"Ajouter un vaccin"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        <TextField id="nom" label="Nom" variant="standard" name="nom" value={vaccine.nom} onChange={handleChange}/>
                        <br />
                        <TextField id="date" label="Date" variant="standard" name="date" value={vaccine.date} onChange={handleChange} helperText="Date au format YYYY-MM-DD"/>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Annuler</Button>
                    <Button onClick={handleSubmit} autoFocus>
                        Ajouter
                    </Button>
                </DialogActions>
            </Dialog> 
        </div>
    )
}

export default Vaccine;