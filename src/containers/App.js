import React from "react";
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Button from '@mui/material/Button';

import Home from "./Home";
import Test from "./Test";
import Vaccine from "./Vaccine";
import Location from "./Location";
import Login from "./Login";
import Logout from "../Logout";

function App() {

    const user = useSelector(state => state.User);
    
    return (
        <Router>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                    <Button variant="h6" component="div" href="/home">
                        CovidAlert
                    </Button>
                {
                    user.data && user.data.isLoggedIn ?
                    <div>
                        <Button color="inherit" href="/tests">Tests</Button>
                        <Button color="inherit" href="/vaccines">Vaccins</Button>
                        <Button color="inherit" href="/locations">Localisation</Button>
                        <Button color="inherit" href="/logout">Logout</Button>
                    </div>
                    : <Button color="inherit" href="/login">Login</Button>

                }
                    </Toolbar>
                </AppBar>
            </Box>
            {/* A <Switch> looks through its children <Route>s and
                renders the first one that matches the current URL. */}
            <Switch>
                <Route exact path="/"><Home /></Route>
                <Route path="/home"><Home /></Route>
                <Route path="/tests"><Test /></Route>
                <Route path="/vaccines" ><Vaccine /></Route>
                <Route path="/locations" ><Location /></Route>
                <Route path="/login" ><Login /></Route>
                <Route path="/logout" ><Logout /></Route>
            </Switch>
              
        </Router>
        
    );
}

export default App;