import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Keycloak from 'keycloak-js';
import {login} from "../actions/UserActions";
import Button from '@mui/material/Button';
import Switch from '@mui/material/Switch';
import Stack from '@mui/material/Stack';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import { deleteTest, postTest, getAllTest } from '../actions/TestActions';
import DeleteIcon from '@mui/icons-material/Delete';
import AddIcon from '@mui/icons-material/Add';
import moment from 'moment';

const Test = () => {


    const initialTest = {nom:"", date:"", is_negative: true}

    const dispatch = useDispatch();
    const user = useSelector(state => state.User);
    const [open,setOpen] = useState(false)
    const [test,setTest] = useState(initialTest)
    const listTestState = useSelector(state => state.Test);
    const [listTest,setlistTest] = useState([]);
    
    useEffect(() => {
        if(user.data && !user.data.isLoggedIn){
            const keycloak = Keycloak('/keycloak.json');
            keycloak.init({onLoad: 'login-required'}).then(authenticated => {
                if(authenticated){
                    const data = { token: keycloak.token, isLoggedIn: authenticated, id_user: keycloak.tokenParsed.sub }
                    dispatch(login(data));
                    dispatch(getAllTest(data.id_user));
                }
            })
        }  
        if(Array.isArray(listTestState.data)){
            setlistTest(listTestState.data)
        }   
    });

    const handleClose = () => {
        setOpen(false);
    };

    const handleChange = (evt) => {
        const { name, value } = evt.target
        setTest({...test, [name]: value})
        
    }

    const handleSubmit = () => {
        console.log(test)
        handleClose()
        dispatch(postTest({...test, id_user: user.data.id_user}))

    }

    const handleDelete = (id) => {
        dispatch(deleteTest(id))
    }

    return (
        <div style={{textAlign:'center'}}>
            <Typography variant="h5" component="div" >Mes tests réalisés</Typography>
            <AddIcon color="success" onClick={()=>setOpen(true)}/>
            <br/>
            {
                (listTest || []).map(t => 
                    <Box
                        component="span"
                        sx={{ display: 'inline-block', mx: '5px', minWidth: '100px', minHeight:'100px'}}
                        key={t.id_test}
                    >
                        <Card variant="outlined">
                        <CardContent>
                            <Typography variant="h5" component="div">
                                {t.nom}
                            </Typography>
                            <Typography color="text.secondary">
                                {moment(t.date).format("DD MMM YYYY")}
                            </Typography>
                            <Typography color="text.secondary">
                                {t.is_negative ? 'Négatif' : 'Positif'}
                            </Typography>
                            </CardContent>
                            <DeleteIcon color="error" onClick={() => handleDelete(t.id_test)}/>
                            
                        </Card>
    
                    </Box>
                )
            }
            
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                {"Ajouter un Test"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">

                        <TextField id="nom" label="Nom" variant="standard" name="nom" value={test.nom} onChange={handleChange}/>
                        <br />
                        <TextField id="date" label="Date" variant="standard" name="date" value={test.date} onChange={handleChange} helperText="Date au format YYYY-MM-DD"/>
                        <Stack direction="row" spacing={1} alignItems="center">
                            <Typography>Négatif</Typography>
                            <Switch id="is_negative" checked={!test.is_negative} onChange={() => setTest({...test, is_negative: !test.is_negative})}/>
                            <Typography>Positif</Typography>
                        </Stack>
                        
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Annuler</Button>
                    <Button onClick={handleSubmit} autoFocus>
                        Ajouter
                    </Button>
                </DialogActions>
            </Dialog> 
        </div>
    )
}

export default Test;