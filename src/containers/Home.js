import React from 'react';

const Home = () => {
    return (
        <div>
            <h3>Bienvenue sur CovidAlert !</h3>
            <br/><br/>
            <p>
            Tous anti-covid fait par des étudiants dans le cadre du projet IWA en IG5
            </p>

            <i>Cette application a été développée par des étudiants en IG5 à Polytech Montpellier</i>
        </div>
    )
}

export default Home;