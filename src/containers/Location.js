import React, { useState } from 'react';
import {useDispatch, useSelector} from "react-redux";
import GoogleMapReact from 'google-map-react';
import RoomIcon from '@mui/icons-material/Room';
import AddLocationAltIcon from '@mui/icons-material/AddLocationAlt';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';

const Marker = () => <RoomIcon sx={{ color: 'red' }} />;
const MarkerCovid = () => <AddLocationAltIcon sx={{ color: 'orange' }} />;

const Location = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.User);

    const initLocation= {lat:43.608294, lng:3.879343}
    const [location, setLocation] = useState(initLocation);
    const [error, setError] = useState("");
    const [listLocations, setListLocations] = useState([]);

    function getLocation(){
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, showError);
        } else { 
            setError("Geolocation is not supported by this browser.")
        }
    }

    function showPosition(position) {
        setLocation({lat:position.coords.latitude, lng:position.coords.longitude})
        //dispatch(postLocation(location))
    }

    function showError(error) {
        switch(error.code) {
            case error.PERMISSION_DENIED:
                setError("User denied the request for Geolocation.")
                break;
            case error.POSITION_UNAVAILABLE:
                setError("Location information is unavailable.")
                break;
            case error.TIMEOUT:
                setError("The request to get user location timed out.")
                break;
            case error.UNKNOWN_ERROR:
                setError("An unknown error occurred.")
                break;
            default:
                setError("")
        }
    }
    
    const handleSuspectLocations = (evt) => {
        setListLocations([...listLocations, {lat: evt.lat, lng:evt.lng}])
    }

    return(
        <div style={{ height: '80vh', width: '80%', marginLeft:'auto', marginRight:'auto' }}>
            <Button onClick={getLocation} variant="contained">Me localiser</Button>
            <br />
            <Box
                component="span"
                sx={{ display: 'inline-block', mx: '5px', minWidth: '50px', minHeight:'50px'}}
            >
                <Card variant="outlined">
                <h5>Légende :</h5>
                Ma localisation actuelle:
                <RoomIcon sx={{ color: 'red' }} />
                <br />
                Mes localisations précédentes :
                <AddLocationAltIcon sx={{ color: 'orange' }} />
                <br />
                Les localisations où il y a eu des cas de covid :
                <AddLocationAltIcon sx={{ color: 'red' }} />
                </Card>
            </Box>
            
            <Typography>Cliquer sur la carte pour renseigner les lieux où vous êtes allés</Typography>
            <GoogleMapReact
                bootstrapURLKeys={{ key: "" }}
                defaultCenter={location}
                defaultZoom={13}
                yesIWantToUseGoogleMapApiInternals
                onClick={handleSuspectLocations}
                >
                {
                    location === initLocation
                    ? <></>
                    : <Marker
                        lat={location.lat}
                        lng={location.lng}
                    />
                }
                
                {
                    listLocations.map((l, index) => <MarkerCovid key={index} lat={l.lat} lng={l.lng} />)
                }
            </GoogleMapReact>
        </div>
    )

}

export default Location;