import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import { Redirect } from 'react-router';
import Keycloak from 'keycloak-js';

const Logout = () => {
    const user = useSelector(state => state.User);
    const [error, setError] = useState("");
  
    useEffect(() => {
        if(user.data && user.data.isLoggedIn){
            const keycloak = Keycloak('/keycloak.json');
            keycloak.logout().catch(err => setError(err.error))
        }
    })

    return (
      <div>
        {
          user.data.isLoggedIn
          ? <p>Veuillez patienter...</p>
          : <Redirect to="/home" />
        }
        <p>{error}</p>
      </div>
    );
  
}
export default Logout;